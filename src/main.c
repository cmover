/* cmover: moves a character over the screen using curses */
#include <stdlib.h>
#include <stdio.h>
#include <curses.h>

static void quit (void);
static void update_screen (void);
static void update_mainview (void);
static void update_statusline (void);
static void check_size (void);
static void create_windows (void);
static int can_move (int, int);
static WINDOW * mainview = NULL;
static WINDOW * statusline = NULL;
static unsigned int lines, cols;	/* size to work with */
static unsigned int x, y;		/* coordinates */
static char c='X';			/* character to use */

int main (int argc, char ** argv)
{
	int input;
	int mv_x, mv_y;

	atexit (quit);

	initscr ();
	noecho ();
	clear ();

	lines = LINES;
	cols = COLS;
	x = cols / 2;
	y = lines / 2;

	create_windows ();
	update_screen ();

	while (1) {
		input = wgetch (mainview);
		mv_x = mv_y = 0;
		switch (input) {
			case 'h':
			case KEY_LEFT:
				mv_x=-1;
				break;
			case 'j':
			case KEY_DOWN:
				mv_y=1;
				break;
			case 'k':
			case KEY_UP:
				mv_y=-1;
				break;
			case 'l':
			case KEY_RIGHT:
				mv_x=1;
				break;
			case 'y':
				mv_x=-1;
				mv_y=-1;
				break;
			case 'u':
				mv_x=1;
				mv_y=-1;
				break;
			case 'b':
				mv_x=-1;
				mv_y=1;
				break;
			case 'n':
				mv_x=1;
				mv_y=1;
				break;
			case 'q':
				exit (0);
				break;
		}
		check_size ();
		if ((mv_x || mv_y) && can_move (mv_x, mv_y)) {
			x += mv_x;
			y += mv_y;
			update_screen();
		}
	}

	return 0;
}

static void quit (void)
{
	endwin ();
	printf ("exiting...\n");
}

static void check_size (void)
{
	if (LINES != lines || COLS != cols) {
		lines = LINES;
		cols = COLS;
		create_windows ();
		clear ();
		if (y >= lines-1) y = lines-2;
		if (x >= cols) x = cols-1;
		update_screen ();
	}
}

/* (re)creates the two windows. this is needed after resizing the screen */
static void create_windows (void)
{
	if (mainview) delwin (mainview);
	if (statusline) delwin (statusline);
	mainview = newwin (lines-1, 0, 0, 0);
	statusline = newwin (0, 0, lines-1, 0);
	keypad (mainview, TRUE); /* make arrow keys usable */
}

static void update_screen (void)
{
	update_mainview ();
	update_statusline ();
}

static void update_mainview (void)
{
	waddch (mainview, ' ');
	mvwaddch (mainview, y, x, c);
	wmove (mainview, y, x);
	wrefresh (mainview);
}

/* width and height are the size of the whole terminal, not just the mainview. */
static void update_statusline (void)
{
	mvwprintw (statusline, 0, 0, "x: %i, y: %i, width: %i, height: %i",
			x, y, cols, lines);
	wclrtoeol (statusline);
	wrefresh (statusline);
}

static int can_move (const int mv_x, const int mv_y)
{
	return (mv_x+x<cols && mv_y+y<lines-1);
}
